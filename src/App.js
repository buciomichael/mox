import './App.css';
import Banner from './components/Banner';
import Home from './components/Home';
import Nav from './components/Nav';
import Product from './components/Product';
import ProductList from './components/ProductList';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { ShoppingCartProvider } from './context/ShoppingCartContext';


function App() {
  return (
    <>
    <ShoppingCartProvider>
      <Banner />
      <BrowserRouter>
          <Nav />
          <Routes>
            <Route path="/" element={<Home />}></Route>
            <Route path="products">
              <Route index element={<ProductList />}></Route>
              <Route path=":productId" element={<Product />}></Route>
            </Route>
          </Routes>
      </BrowserRouter>
    </ShoppingCartProvider>
    </>
  );
}

export default App;
