import prd1 from "../images/products/product-1.jpg"
import prd2 from "../images/products/product-2.jpg"
import prd2a from "../images/products/product-2-a.jpg"
import prd2b from "../images/products/product-2-b.jpg"
import prd2c from "../images/products/product-2-c.jpg"
import prd3 from "../images/products/product-3.jpg"
import prd4 from "../images/products/product-4.jpg"
import prd5 from "../images/products/product-5.jpg"
import prd6 from "../images/products/product-6.jpg"
import prd7 from "../images/products/product-7.jpg"
import prd8 from "../images/products/product-8.jpg"
import prd9 from "../images/products/product-9.jpg"

export const products = [
    {
        id: 1,
        category: "Kitchen",
        name: "Container",
        img1: prd1,
        description: "this is the pink brush description",
        features: [
            {"title": "Sustainability First",
             "description": "Our Cleaning Hand Brush is made from 100% recycled plastic, making it a sustainable choice for environmentally-conscious individuals. By choosing this brush, you're contributing to the reduction of plastic waste in landfills and oceans."
            },
            {"title": "Effective Cleaning",
            "description": "Despite its eco-friendly construction, this hand brush is highly effective at sweeping away dirt, dust, and debris from various surfaces. The bristles are firm yet gentle, making it suitable for a wide range of cleaning tasks."
           },
           {
            "title": "Comfortable Grip",
            "description": "The ergonomic handle ensures a comfortable grip, reducing hand fatigue during extended cleaning sessions."
           },
           {
            "title": "Versatile Usage",
            "description": "Whether you're tidying up your kitchen, bathroom, or any other area in your home, this cleaning hand brush is up to the task. It's a versatile cleaning tool that you'll reach for again and again.",
           }
        ],
        care: [
            "Made from 100% recycled plastic, showcasing our commitment to sustainability",
            "To maintain its eco-friendliness, simply rinse the bristles and let them air dry after use"
        ],
        size: [
            'This cleaning hand brush measures 10" in length, providing a comfortable size for everyday use.',
            "It's lightweight at just 0.25 lbs, making it easy to handle and maneuver"
        ],
        price: 8.99,
    },
    {
        id: 2,
        category: "Kitchen",
        name: "Cleaning Hand Brush",
        img1: prd2,
        img1a: prd2a,
        img1b: prd2b,
        img1c: prd2c,
        description: "this is the pink brush description",
        features: [
            {"title": "Sustainability First",
             "description": "Our Cleaning Hand Brush is made from 100% recycled plastic, making it a sustainable choice for environmentally-conscious individuals. By choosing this brush, you're contributing to the reduction of plastic waste in landfills and oceans."
            },
            {"title": "Effective Cleaning",
            "description": "Despite its eco-friendly construction, this hand brush is highly effective at sweeping away dirt, dust, and debris from various surfaces. The bristles are firm yet gentle, making it suitable for a wide range of cleaning tasks."
           },
           {
            "title": "Comfortable Grip",
            "description": "The ergonomic handle ensures a comfortable grip, reducing hand fatigue during extended cleaning sessions."
           },
           {
            "title": "Versatile Usage",
            "description": "Whether you're tidying up your kitchen, bathroom, or any other area in your home, this cleaning hand brush is up to the task. It's a versatile cleaning tool that you'll reach for again and again.",
           }
        ],
        care: [
            "Made from 100% recycled plastic, showcasing our commitment to sustainability",
            "To maintain its eco-friendliness, simply rinse the bristles and let them air dry after use"
        ],
        size: [
            'This cleaning hand brush measures 10" in length, providing a comfortable size for everyday use.',
            "It's lightweight at just 0.25 lbs, making it easy to handle and maneuver"
        ],
        price: 9.99,
    },
    {
        id: 3,
        category: "Kitchen",
        name: "Cleaning Brush",
        img1: prd3,
        description: "this is the pink brush description",
        price: "10.00 USD",
    },
    {
        id: 4,
        category: "Kitchen",
        name: "Cleaning Brush",
        img1: prd4,
        description: "this is the pink brush description",
        price: "10.00 USD",
    },
    {
        id: 5,
        category: "Kitchen",
        name: "Cleaning Brush",
        img1: prd5,
        description: "this is the pink brush description",
        price: "10.00 USD",
    },
    {
        id: 6,
        category: "Kitchen",
        name: "Cleaning Brush",
        img1: prd6,
        description: "this is the pink brush description",
        price: "10.00 USD",
    },
    {
        id: 7,
        category: "Kitchen",
        name: "Cleaning Brush",
        img1: prd7,
        description: "this is the pink brush description",
        price: "10.00 USD",
    },
    {
        id: 8,
        category: "Kitchen",
        name: "Cleaning Brush",
        img1: prd8,
        description: "this is the pink brush description",
        price: "10.00 USD",
    },
    {
        id: 9,
        category: "Kitchen",
        name: "Cleaning Brush",
        img1: prd9,
        description: "this is the pink brush description",
        price: "10.00 USD",
    },
]
