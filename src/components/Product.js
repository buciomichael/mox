import React from 'react'
import './Product.css';
import { Link, useParams } from 'react-router-dom';
import { products } from './Data';
import { useShoppingCart } from '../context/ShoppingCartContext';

function Product() {
    const { getItemQuantity,
            increaseCartQuantity,
            decreaseCartQuantity,
            removeFromCart
        } = useShoppingCart()
    const { productId } = useParams();
    const product = products.find((product) => product.id === Number(productId));
    const { id, img1, img1a, img1b, img1c, name, price, features, care, size} = product;
    const quantity = getItemQuantity(id);

  return (
    <>
    <div className='single-product-container'>
        <div className='product-grid'>
            <div className='images-row-1'>
                <img src={img1}></img>
                <img src={img1a}></img>
            </div>
            <div className='images-row-2'>
                <img src={img1b}></img>
                <img src={img1c}></img>
            </div>
            <div className='details-container'>
                <div className='detail-top'>
                     <div className='product-detail-name'>{name}</div>
                     <div className='product-detail-price'>{price}</div>
                  </div>
                <div className='detail-middle'>
                    <div className='color-selection'>
                        <div className='color-text'>
                            <p className='color'>Color: <span>Pink</span></p>
                        </div>
                        <div className='color-icon-container'>
                            <div className='color-icon'></div>
                        </div>
                    </div>
                    <div className='add-to-bag'>
                        <div className='quantity-container'>
                            <button className='decrement-button'>-</button>
                            <div className='quantity'>{quantity}</div>
                            <button className='increment-button'>+</button>
                        </div>
                        <button className='add-bag-button' onClick={() => increaseCartQuantity(id)}>ADD TO BAG</button>
                    </div>
                </div>
                <div className='detail-bottom'>
                    <h3>Features:</h3>
                    <ul className='features-list'>
                    {features.map((feature, index) => (
                        <li key={index}>
                            <h4>{feature.title}</h4>
                            <p>{feature.description}</p>
                        </li>
                        ))}
                    </ul>
                    <h3>Content + Care:</h3>
                    <ul className='care-list'>
                    {care.map((instruction, index) => (
                        <li key={index}>
                            {instruction}
                        </li>
                        ))}
                    </ul>
                    <h3>Size:</h3>
                    <ul className='care-list'>
                    {size.map((item, index) => (
                        <li key={index}>
                            {item}
                        </li>
                        ))}
                    </ul>
                </div>
            </div>
            <div className='side-banner'></div>
        </div>
    </div>
    </>
  )
}

export default Product
