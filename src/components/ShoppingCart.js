import React, { useEffect, useRef } from 'react'
import './ShoppingCart.css';
import { useShoppingCart } from '../context/ShoppingCartContext'
import CartItem from './CartItem';

function ShoppingCart() {
  const { closeCart, isOpen, cartQuantity, cartItems } = useShoppingCart()
  return (
    <div className='shopping-cart-container' show={isOpen} onHide={closeCart}>
      <div className='shopping-cart-top'>
        <div className='my-bag'>
          <span>My Bag</span>
          <span>({ cartQuantity })</span>
        </div>
        <div className='close-cart-button' onClick={closeCart}>
          X
        </div>
      </div>
      <div className='shopping-cart-middle'>
          {cartItems.map(item => (
          <CartItem key={item.id} {...item} />
          ))}
      </div>
      <div className='shopping-cart-bottom'></div>
    </div>
  )
}

export default ShoppingCart
