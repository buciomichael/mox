import React from 'react'
import './CartItem.css';
import { useShoppingCart } from '../context/ShoppingCartContext'
import { products } from './Data'

function CartItem({ id, quantity }) {

    const { removeFromCart, getItemQuantity } = useShoppingCart()
    const item = products.find(i => i.id === id)
    if (item == null) return null

  return (
    <div className='cart-item-container'>
        <div>
            <img src={item.img1} style={{ width: '100px' }} />
        </div>
        <div>
            {item.name}
        </div>
        <div>
            {item.price * quantity}
        </div>
        <div>
            {quantity}
        </div>
    </div>
  )
}

export default CartItem
