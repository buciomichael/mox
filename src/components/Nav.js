import { useShoppingCart } from '../context/ShoppingCartContext';
import './Nav.css';
import { NavLink } from 'react-router-dom';
import ShoppingCart from './ShoppingCart';

function Nav() {

  const { isOpen, setIsOpen, cartQuantity } = useShoppingCart()

  return (
    <div className='navigation-container'>
      <div className='main-nav'>
        <div className='logo'>
          <NavLink to="/">MOX</NavLink>
        </div>
        <div className='nav-bar'>
          <ul className='nav-list'>
            <li>
              <NavLink to="/products">SHOP</NavLink>
            </li>
            <li><a href=''>ABOUT US</a></li>
            <li><a href=''>CONTACT</a></li>
            <li><a href=''>LOCATE US</a></li>
          </ul>
        </div>
        <div className='search-cart'>
          <div className='search-container'>
            <form className='search-form'>
              <input className='search-input' placeholder='Search' type='text'></input>
              <button className='search-button'>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
                  <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                </svg>
              </button>
            </form>
          </div>
          <div className='cart-icon-container'>
            {cartQuantity > 0 && (
            <button className='cart-quantity-icon' onClick={()=>{setIsOpen(!isOpen)}}>{cartQuantity}</button>
            )}
            <button className='cart-button' onClick={()=>{setIsOpen(!isOpen)}}>
            <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-bag" viewBox="0 0 16 16">
              <path d="M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1zm3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4h-3.5zM2 5h12v9a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V5z"/>
            </svg>
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Nav
