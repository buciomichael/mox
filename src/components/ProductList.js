import { products } from './Data';
import React from 'react'
import { Link } from 'react-router-dom';
import './ProductList.css';
import img1 from "../images/products/advertisement-1.jpg"


function ProductList() {
    return (
        <>
        <div className='product-list-container'>
          <div className='product-list-grid'>
            <section className='side-nav'>
            <ul>
                  <div className='browse'>
                    <li>Browse by:</li>
                  </div>
                  <li>All Products</li>
                  <li>Kitchen</li>
                  <li>Bathroom</li>
                  <li>Cleaning</li>
                  <li>Outdoors</li>
                  </ul>
            </section>
            <section className='products-row-1'>
              <div className='advertisement-1 product-img-container'>
                <img className='advertisement-img' src={img1}></img>
              </div>
              {products.map(product => {
                if(product.id === 1 || product.id === 2) {
                  return (
                    <div className="product-container" key={product.id}>
                      <div>
                        <Link to={`/products/${product.id}`}>
                          <img className='product-img' src={product.img1} alt="product1" />
                        </Link>
                      </div>
                      <div className='product-name'>
                        <Link to={`/products/${product.id}`}>{product.name}</Link>
                      </div>
                      <div className='product-price'>{product.price}</div>
                    </div>
                );
                }
            })}
            </section>
            <section className='products-row-2'>
              {products.map(product => {
                if(product.id === 3 || product.id === 4 || product.id === 5 || product.id === 6) {
                  return (
                    <div className="product-container" key={product.id}>
                      <img className='product-img' src={product.img1} alt="product1" />
                      <div className='product-name'>{product.name}</div>
                      <div className='product-price'>{product.price}</div>
                    </div>
                );
                }
            })}
            </section>
            <section className='products-row-3'>
              {products.map(product => {
                if(product.id === 7 || product.id === 8 || product.id === 9) {
                  return (
                    <div className="product-container" key={product.id}>
                      <img className='product-img' src={product.img1} alt="product1" />
                      <div className='product-name'>{product.name}</div>
                      <div className='product-price'>{product.price}</div>
                    </div>
                );
                }
            })}
            </section>
          </div>
        </div>
        </>
      );
}

export default ProductList
